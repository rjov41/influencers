$(document).ready(function () {
    fnfilterByhash();
    fnSetClickEvent();
    fnFiltrarCursos(1);
});

function fnfilterByhash() {
    var hash = window.location.hash.substring(1);
    $query = $('[data-filter="' + hash + '"]');
    if (hash && $query.length) {
        $('.course').hide();
        $('.filter-cat-' + hash).show();
        $element = $('[data-filter="' + hash + '"]');
        $parent = $element.parent();
        $grandContent = $('.filter-buttons');
        hassubcats = $element.hasClass('has-subcats');
        isCat = $element.hasClass('card-cat');
        datafilter = $element.attr('data-filter');
        $('[data-filter]').parent().removeClass('active');
        $parent.addClass('active');
        if (hassubcats && isCat) {
            $grandContent.addClass('expand');
        } else {
            $grandContent.removeClass('expand');
        }
        if (!isCat) {
            $element.parents('.cat').addClass('active');
            $grandContent.addClass('expand');
        }
        fnFiltrarCursos(datafilter);
    } else {
        fnFiltrarCursos('*');
    }

    return false;
}

function fnFiltrarCursos(datafilter) {
    $('.course').hide();
    if (datafilter === '*') {
        $('.course').show();
        $('[data-filter="*"]').parent().addClass('active');
    } else {
        $('.filter-cat-' + datafilter).show();
        window.location.hash = datafilter;
    }
    return false;
}

function fnSetClickEvent() {

    $('[data-filter]').on('click', function (e) {
        e.preventDefault();
        $element = $(this);
        $parent = $element.parent();
        $grandContent = $('.filter-buttons');
        hassubcats = $element.hasClass('has-subcats');
        isCat = $element.hasClass('card-cat');
        datafilter = $element.attr('data-filter');

        if (!$parent.hasClass('active')) {
            $('[data-filter]').parent().removeClass('active');
            $parent.addClass('active');

            if (hassubcats && isCat) {
                $grandContent.addClass('expand');
            } else {
                $grandContent.removeClass('expand');
            }
            if (!isCat) {
                $element.parents('.cat').addClass('active');
                $grandContent.addClass('expand');
            }

            fnFiltrarCursos(datafilter);

            $('[data-filter="' + datafilter + '"]').parent().addClass('active');
            if ($('body').width() < 972) {
                $([document.documentElement, document.body]).animate({
                    scrollTop: ($("#courseList").offset().top - 20)
                }, 700);
            }

        } else {
            $('[data-filter]').parent().removeClass('active');
            $grandContent.removeClass('expand');
        }

    });

    window.addEventListener('popstate', function (event) {
        fnfilterByhash();
    }, false);

    return false;
}