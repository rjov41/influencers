$(document).ready(function () {
    $(".oratoria").hide(400);
    $(".styling").hide(400);
    $(".tutoriales").hide(400);
    $(".contenido").hide(400);

        $(".btn-oratoria").click(function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
            $(".oratoria").show(400);
            $(".banner").css({'background-image': 'url('+BASE_URL+'lib/img/fondo-2.png)','height':'300px'});
            $(".menu").find(".active").removeClass("active");
            $(".menu .btn-oratoria").addClass("active");
            $(".bienvenida").hide(400);
            $(".styling").hide(400);
            $(".tutoriales").hide(400);
            $(".contenido").hide(400);
            $(".seccion").hide(400);
            $.ajax({
                type: "post",
                url: BASE_URL + "track",
                data: {"category": "tips_redes_sociales"},
            });
        });

        $(".btn-styling").click(function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
            $(".styling").show(400);
            $(".banner").css({'background-image': 'url('+BASE_URL+'lib/img/fondo-2.png)','height':'300px'});
            $(".menu").find(".active").removeClass("active");
            $(".menu  .btn-styling").addClass("active");
            $(".bienvenida").hide(400);
            $(".oratoria").hide(400);
            $(".tutoriales").hide(400);
            $(".contenido").hide(400);
            $(".seccion").hide(400);
            $.ajax({
                type: "post",
                url: BASE_URL + "track",
                data: {"category": "videos"},
            });
        });

        $(".btn-tutoriales").click(function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
            $(".tutoriales").show(400);
            $(".banner").css({'background-image': 'url('+BASE_URL+'lib/img/fondo-2.png)','height':'300px'});
            $(".menu").find(".active").removeClass("active");
            $(".menu .btn-tutoriales").addClass("active");
            $(".bienvenida").hide(400);
            $(".oratoria").hide(400);
            $(".styling").hide(400);
            $(".contenido").hide(400);
            $(".seccion").hide(400);
        });

        $(".btn-contenido").click(function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
            $(".contenido").show(400);
            $(".banner").css({'background-image': 'url('+BASE_URL+'lib/img/fondo-2.png)','height':'300px'});
            $(".menu").find(".active").removeClass("active");
            $(".menu .btn-contenido").addClass("active");
            $(".bienvenida").hide(400);
            $(".tutoriales").hide(400);
            $(".oratoria").hide(400);
            $(".styling").hide(400);
            $(".seccion").hide(400);
            $.ajax({
                type: "post",
                url: BASE_URL + "track",
                data: {"category": "contenido"},
            });
        });

        $(".btn-home").click(function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
            $(".seccion").show(400);
            $(".bienvenida").show(400);
            $(".banner").css({'background-image': 'url('+BASE_URL+'lib/img/Untitled-1.jpg)','height':'480px'});
            $(".menu").find(".active").removeClass("active");
            $(".menu .btn-home").addClass("active");
            $(".tutoriales").hide(400);
            $(".oratoria").hide(400);
            $(".styling").hide(400);
            $(".contenido").hide(400);
            $.ajax({
                type: "post",
                url: BASE_URL + "track",
                data: {"category": "home"},
            });
        });

        $(".logo").click(function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
            $(".seccion").show(400);
            $(".bienvenida").show(400);
            $(".banner").css({'background-image': 'url('+BASE_URL+'lib/img/Untitled-1.jpg)','height':'480px'});
            $(".menu").find(".active").removeClass("active");
            $(".menu .btn-home").addClass("active");
            $(".tutoriales").hide(400);
            $(".oratoria").hide(400);
            $(".styling").hide(400);
            $(".contenido").hide(400);
            $.ajax({
                type: "post",
                url: BASE_URL + "track",
                data: {"category": "home"},
            });
        });


        $(".carta-header .open").click(function (e) {
            e.preventDefault();
            $(".galery").show();
        });


        	/* This is basic - uses default settings */
            $("a#single_image").fancybox();
            /* Using custom settings */
            $("a#inline").fancybox({
                'hideOnContentClick': true
            });

            /* Apply fancybox to multiple items */
            $("a.group").fancybox({
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'speedIn'		:	600, 
                'speedOut'		:	200, 
                'overlayShow'	:	false
            });

});

