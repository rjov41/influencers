jQuery(document).ready(function($) {
	$(function () {
		$('#loginform').submit(function (event) {
			event.preventDefault();

            $('#error-code').addClass("hidden");
			$('#error-pass').addClass("hidden");
			var data = new Object();
			data.code = $("#usuario").val();
			data.pass = $("#password").val();
			var error = false;
			if (data.code === "") {
				$('#error-code').removeClass("hidden");
				error = true;
			}
			if (!error) {

				$.blockUI();
				NProgress.start();
				$.ajax({
					type: 'POST',
					url: base_url+'Login_controller/validation',
					data: data,
					success: function (res) {
						$.unblockUI();
						NProgress.done();
						var response = JSON.parse(res);
						if(response.error != 0){
							$('.alert_login').removeClass('hidden');
							$('.form-group').addClass('error');
						} else {
							window.location.href = base_url;
						}
					},
					error:function (response) {
						$.unblockUI();
						NProgress.done();
					}
				});
			}
		});
	});	
});