<?php 
if(!function_exists("user_data")){
    function user_data($name_session)
    {
        $CI =& get_instance();
        if ( $CI->session->userdata( $name_session) ) {
            return $CI->session->userdata( $name_session);
        } else {
            return FALSE;
        }
    }
}
?>