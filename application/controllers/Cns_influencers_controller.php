<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Cns_influencers_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ( !user_data("user_login")) {
            redirect( base_url() . 'ingreso' );
        }

    }


	public function index()
	{
        $this->load->model( "Tracks_model" );
        $trackVO           = new TrackModelVO();
        $trackVO->action   = "page_view";
        $trackVO->category = "home_view";
        $trackVO->label    = "";
        $this->Tracks_model->track( $trackVO );

		$this->load->view('cns_influencers_view');
    }
    
    public function track()
    {
        $this->load->model( "Tracks_model" );
        $trackVO           = new TrackModelVO();
        $trackVO->action   = "page_view";
        $trackVO->category = $this->input->post('category');
        $trackVO->label    = "";
        $this->Tracks_model->track( $trackVO );
    }
}
