<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Cns_login_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Login_model");
    }


	public function index()
	{
        if ( user_data("user_login") ) {
            $this->session->sess_destroy();
        }
		$this->load->view('cns_login_view');
    }
    
    public function access(){
        $code =  $this->input->post("codigo");

        $access = $this->validation($code);
        echo json_encode($access);
    }

    private function validation($code){
        
        if($code == 99999999){ // Que sea admin

            $array = array(
                'user_login' => 'true',
                'user_name' => "admin",
                'user_code' => 99999999,
            );
            $this->session->set_userdata( $array );
        }else{

            $data_valid = $this->Login_model->validation_user($code);
            // print_r ("a:".$data_valid);
            if(!empty($data_valid)){
                $array = array(
                    'user_login' => 'true',
                    'user_name' => $data_valid[0]->nombre,
                    'user_code' => $data_valid[0]->code,
                );

                $this->session->set_userdata( $array );
            }else{
                // print_r ("b:".$data_valid);
                return false;
            }
        }
        return true;
    }
}
