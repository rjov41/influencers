<?php

/**
 * Created by PhpStorm.
 * User: ferxyclon
 * Date: 12/14/15
 * Time: 9:47 PM
 */
class Tracks_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function track(TrackModelVO $vo)
    {
        $data = array(
            "user_code" => user_data("user_code"),
            "username" => user_data("user_name"),
            "cycle_id" => 0, // no se usa ciclos en el site
            "category" => $vo->category,
            "action" => $vo->action,
            "label" => $vo->label,
            'user_agent' => $this->input->user_agent(),
            'ip' => $this->input->ip_address()
        );
        if(!$this->session->userdata("isXyclon"))
        {
            $this->db->insert("tracks_influencers", $data);
        }
    }
}