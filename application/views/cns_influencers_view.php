<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Influencers</title>
    <link rel="stylesheet" href="<?php echo base_url();?>lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>lib/css/ff-helvetica_fonts.css">
    <link rel="stylesheet" href="<?php echo base_url();?>lib/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>lib/css/influencers.css">
</head>
<body>

<body>
<div class="influencers">
    <!-- menu -->
    <div class="header">
        <div class="contenedor">
            <div class="col-md-3 header1">
                <img class="logo" src="<?php echo base_url();?>lib/img/logo.png" alt="">
            </div>
            <div class="col-md-7 header2">
                <!-- <span class="frase">El mundo es mas bonito con vos</span> -->
            </div>
            <div class="col-md-2 header3">
            <div class="dropdown">
                <img class="user" src="<?php echo base_url();?>lib/img/user.png" alt="">
                    <div class="dropdown-content">
                        <div class="dropdown-header">
                            <div class="admin">
                                <img src="<?php echo base_url();?>lib/img/user.png" alt="">
                                <span class="name-user"><?php echo user_data("user_name"); ?></span>
                            </div>
                            <a href="<?php echo base_url();?>ingreso" class="salir">Salir</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-bottom"></div>
    </div>
   <!--  fin menu -->
   <!-- banner -->
   <div class="banner" style="background-image: url(<?php echo base_url();?>lib/img/Untitled-1.jpg)">
    <div class="contenido-influencer">
        <!-- menu -->
        <div class="menu">
            <span class="btn-home active">Home</span>
            <span class="btn-oratoria">Tips para tus redes sociales</span>
            <span class="btn-styling">Videos</span>
            <!-- <span class="btn-tutoriales">Tutoriales</span> -->
            <span class="btn-contenido">Contenido</span>
        </div>
        <!-- fin menu -->
        <!-- bienvenida -->
        <div class="bienvenida">
            <span>¡Bienvenida!</span>
            <p>Desde Natura queremos acompañarte para potenciar tu perfil en redes sociales. 
                En esta página te vamos a compartir información exclusiva para vos, 
                para poder acompañarte en este mundo digital.

            </p>
        </div>
        <!-- fin bienvenida -->
       <!--  home -->
        <div class="seccion">
            <div class="cartas col-md-6">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/card-header-1.png)">
                        <span>Tips para tus redes sociales</span>
                    </div>
                    <div class="carta-contenido ">
                        <!-- <p>Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi
                            ut aliquip ex ea commodo consequat.
                        </p> -->
                        <a href="#" class="btn-oratoria ingresar">Ingresar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/card-header-2.png)">
                        <span>Videos</span>
                    </div>
                    <div class="carta-contenido">
                        <!-- <p>Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi
                            ut aliquip ex ea commodo consequat.
                        </p> -->
                        <a href="#" class="btn-styling ingresar">Ingresar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/card-header-3.png)">
                        <span>Tutoriales</span>
                    </div>
                    <div class="carta-contenido">
                        <!-- <p>Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi
                            ut aliquip ex ea commodo consequat.
                        </p> -->
                        <a href="#" class="btn-tutoriales ingresar">Ingresar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/card-header-4.png)">
                        <span>Contenido</span>
                    </div>
                    <div class="carta-contenido">
                        <!-- <p>Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi
                            ut aliquip ex ea commodo consequat.
                        </p> -->
                        <a href="#" class="btn-contenido ingresar">Ingresar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin home -->
        <!-- tips oratoria -->
        <div class="oratoria">
            <div class="cartas col-md-6">
                <div class="carta">
                    <a id="single_image" href="<?php echo base_url();?>lib/img/tips/Diapositiva1.JPG" data-caption="Te compartimos algunas ideas para empezar a organizar tus posteos en redes sociales">
                        <div class="carta-header">
                            <img class="fondo" src="<?php echo base_url();?>lib/img/tips/Diapositiva1.JPG" alt="">
                            <img class="open" src="<?php echo base_url();?>lib/img/insta.png" alt="">
                        </div>
                    </a>
                    <!-- <a id="fancybox-close">
                        <img src="<?php //echo base_url();?>lib/img/fancy_close.png" alt="">
                    </a> -->
                    <!-- <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="<?php //echo base_url();?>lib/img/tips/Diapositiva1.JPG" download="Diapositiva1">Descargar</a>
                    </div> -->
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-02.png)">
                        <img src="<?php echo base_url();?>lib/img/yt.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-03.png)">
                        <img src="<?php echo base_url();?>lib/img/hamb.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-04.png)">
                        <img src="<?php echo base_url();?>lib/img/yt.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin tips oratoria -->
        <!-- videos -->
        <div class="styling">
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-01.png)">
                        <img src="<?php echo base_url();?>lib/img/insta.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6">
                <div class="carta">
                    <a class="iframe" id="single_image" href="https://www.youtube.com/embed/2FPFapx6sMs" data-caption="Capacitación exclusiva para nuestros consultores de la mano de la Influencer Marou Rivero">
                        <div id="video-yt" class="carta-header">
                            <img class="fondo" src="<?php echo base_url();?>lib/img/oratorio-02.png" alt="">
                            <img class="open" src="<?php echo base_url();?>lib/img/yt.png" alt="">
                        </div>
                    </a>
                    <!-- <div class="carta-contenido secciones">
                        <a href="<?php echo base_url();?>lib/img/videos/Entrenamiento-Digital.mp4" download="Entrenamiento-Digital">Descargar</a>
                    </div> -->
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-03.png)">
                        <img src="<?php echo base_url();?>lib/img/hamb.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-04.png)">
                        <img src="<?php echo base_url();?>lib/img/yt.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin videos -->
        <!-- Tutoriales -->
        <div class="tutoriales hidden">
            <div class="cartas col-md-6">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-01.png)">
                        <img src="<?php echo base_url();?>lib/img/insta.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-02.png)">
                        <img src="<?php echo base_url();?>lib/img/yt.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-03.png)">
                        <img src="<?php echo base_url();?>lib/img/hamb.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-04.png)">
                        <img src="<?php echo base_url();?>lib/img/yt.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin tutoriales -->
        <!-- contenido -->
        <div class="contenido">
            <div class="cartas col-md-6">
                <div class="carta">

                    <a id="single_image" href="<?php echo base_url();?>lib/img/contenido/Diapositiva2.JPG" data-caption="Te compartimos algunas ideas para empezar a organizar tus posteos en redes sociales">
                        <div class="carta-header">
                            <img class="fondo" src="<?php echo base_url();?>lib/img/contenido/Diapositiva2.JPG" alt="">
                            <img class="open" src="<?php echo base_url();?>lib/img/insta.png" alt="">
                        </div>
                    </a>
                    <!-- <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="<?php // echo base_url();?>lib/img/contenido/Diapositiva2.JPG" download="Diapositiva2">Descargar</a>
                    </div> -->
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-02.png)">
                        <img src="<?php echo base_url();?>lib/img/yt.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-03.png)">
                        <img src="<?php echo base_url();?>lib/img/hamb.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
            <div class="cartas col-md-6 hidden">
                <div class="carta">
                    <div class="carta-header" style="background-image: url(<?php echo base_url();?>lib/img/oratorio-04.png)">
                        <img src="<?php echo base_url();?>lib/img/yt.png" alt="">
                    </div>
                    <div class="carta-contenido secciones">
                        <span class="material">Material Fotográfico</span>
                        <span class="descrip">Texto Descriptivo</span>
                        <a href="#">Descargar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin contenido -->

    </div>
   </div>

   <div class="galery hidden">
        <div class="opacity"></div>
        <div class="content-galery">
            <div class="content-image">
                <div>
                    <span class="cerrar"></span>
                    <img src="<?php echo base_url();?>lib/img/tips/Diapositiva1.JPG" class="img-ficha img-responsive" alt="">
                </div>
            </div>
        </div>
   </div>
   <!-- fin banner -->
</div>
<script> const BASE_URL = "<?php echo base_url();?>";</script>
<script>const HREF = "https://www.youtube.com/embed/2FPFapx6sMs"</script>
<script src="<?php echo base_url();?>lib/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url();?>lib/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>lib/js/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url();?>lib/js/influencers.js"></script>
</body>
</html>
