<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Influencers</title>
    <link rel="stylesheet" href="<?php echo base_url();?>lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>lib/css/ff-helvetica_fonts.css">
    <link rel="stylesheet" href="<?php echo base_url();?>lib/css/influencers.css">
</head>
<body>
    <div class="container-login">
        <div class="content-login col-md-6">
            <div class="logo-header">
                <div class="login-contenido">
                    <img class="logo" src="<?php echo base_url();?>lib/img/logo.png" alt="">
                </div>
                <div class="frase-login">
                </div>
            </div>
            <div class="form-login">
                <form action="#" class="form" id="form">
                    <label for="">Código CN</label>
                    <input type="number" required="" class="form-control codigo" >
                    <span class="alert_login e_codigo" style="display:none;">Usuario Incorrecto</span>
                    <br>
                    <!-- <label for="">Contraseña</label>
                    <input type="text" class="form-control"> -->
                    <input type="submit" value="Ingresar" class="form-control">
                </form>
            </div>
        </div>
        <div class="banner-login col-md-6" style="background-image: url(<?php echo base_url();?>lib/img/fondo-login-01.png)">
        </div>
    </div>
<body>
<script> const BASE_URL = "<?php echo base_url(""); ?>";</script>
<script src="<?php echo base_url();?>lib/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url();?>lib/js/bootstrap.min.js"></script>
<!-- <script src="<?php // echo base_url();?>lib/js/influencers.js"></script> -->

<script>
    $(document).ready(function () {
        $("#form").on("submit", function () {
            let codigo = $(".codigo").val();

            $.ajax({
                type: "post",
                url: BASE_URL+"login/acceso",
                dataType: "json",
                data: {"codigo":codigo},
                success: function (response) {
                    if(response){
                        window.location="home";
                }else{
                    $(".e_codigo").css("display","block");
                    }
                    
                }
            });
            return false;
        });
    });
</script>
</body>
</html>
